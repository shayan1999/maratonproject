/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './navi/mainNavigate';
import { name as appName } from './app.json';
import Login from './comps/Login'

AppRegistry.registerComponent(appName, () => App);
//AppRegistry.registerComponent(appName, () => Login);

