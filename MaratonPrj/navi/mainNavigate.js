import { createAppContainer } from 'react-navigation';
// import LogIn from '../components/Login'
import { createStackNavigator } from 'react-navigation-stack'
import Side from '../comps/sideComponent'
import Splash from '../comps/SplashScreen'
import LogIn from '../comps/Login'

const MainNavigator = createStackNavigator({
    Splash: {
        screen: Splash
    },
    LogIn: {
        screen: LogIn
    },

    Side: {
        screen: Side
    }

});

const PrevGetStackForAction = MainNavigator.router.getStateForAction;
MainNavigator.router = {
    ...MainNavigator.router,
    getStateForAction(action, state) {
        if (state && action.type === 'ReplaceCur') {
            const routes = state.routes.slice(0, state.routes.length - 1);
            routes.push(action);
            return {
                ...state,
                routes,
                index: routes.length - 1,
            }
        }
        return PrevGetStackForAction(action, state);
    }
}

const App = createAppContainer(MainNavigator);

export default App;