import { createAppContainer, createStackNavigator } from 'react-navigation';
import React from 'react'
import { createDrawerNavigator } from 'react-navigation-drawer';
import {Platform, Dimensions} from 'react-native'

import HomeScreen from '../comps/HomePage';
import MenuDrawer from '../comps/MenuDrawer'
import Suggest from '../comps/Suggest'
import No from '../comps/NotReady'


const Width = Dimensions.get('window').width;

const DrawerConfig = {
    drawerWidth: Width*0.8,
    contentComponent: ({navigation}) =>{
      return(<MenuDrawer navigation={navigation}/>)
    }
} 

const DrawerNavigator = createDrawerNavigator(
  {
    Home:{
        screen: HomeScreen
    },
    Suggest:{
      screen: Suggest
    },
    No:{
      screen: No
    }
  },
DrawerConfig
);

export default createAppContainer(DrawerNavigator)