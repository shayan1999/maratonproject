import React from 'react';
import people from '../pics/people.png';
import home from '../pics/home.png';
import star from '../pics/star.png';
import back from '../pics/arrow.png';
import mapic from '../pics/map.png'
import MenuBut from './MenuBar'
import Parse from 'parse/react-native';
import MapView from 'react-native-maps'
import { Marker } from 'react-native-maps'
import RetroMapStyle from '../MapStyles/RetroMapStyle'
import { Button } from 'native-base'
import { AsyncStorage } from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
Parse.setAsyncStorage(AsyncStorage);
Parse.serverURL = 'https://parseapi.back4app.com/';
Parse.initialize("ImoJxywZ9Fre8V8vpFyHjwtpewOArNzOvtToDmcA", "qW0uuIsFwvelWUUi1ORdo79ywIsVCryePRTTAQa5");

import {
  SafeAreaView,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
  Dimensions
} from 'react-native';

const getFlatListData = require('../Data/FlatListData');
const getFlatListData2 = require('../Data/CommentList')
//screen width
const widthAll = Dimensions.get('screen').width;

class FlatListItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeRowKey: null,
    };
  }
  render() {
    return (
      //making cards here
      <View style={{ flex: 1, backgroundColor: 'grey' }}>
        <LinearGradient colors={['#06beb6', '#48b1bf', '#06beb6']} style={Styles.linearGradient}>
          <View style={{ display: 'flex', width: '100%', height: 350, flexDirection: 'column', alignItems: 'center', borderRadius: 12, marginTop: 4, backgroundColor: '#1C6977', shadowOpacity: 1 }}>
            <TouchableOpacity style={{ flex: 5, width: '80%', marginTop: 16, display: 'flex' }} onPress={() => this.props.parentFlatList.firstGap(this.props.item)}>
              <Image source={{ uri: this.props.item.imageUrl }} style={{ flex: 1 }} />
            </TouchableOpacity>
            <View style={{ flex: 1, display: 'flex', flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ borderWidth: 0, backgroundColor: '#519FB5', paddingRight: 8, borderRadius: 6, marginTop: -24, marginLeft: 16, fontSize: 20, color: 'white', textAlign: 'center' }}>
                  {this.props.item.name}
                </Text>
              </View>
              <View style={{ flex: 2 }}>

              </View>
            </View>
            <View style={{ flex: 2, display: 'flex', flexDirection: 'row', width: '90%', borderColor: '#519FB5', borderWidth: 3, borderRadius: 8, alignItems: 'center', marginBottom: 8, marginTop: -20, backgroundColor: '#C6D9DE' }}>
              <View style={{ flex: 1, borderRightWidth: 0.5, borderRightColor: 'black', display: 'flex', flexDirection: 'row' }}>
                <Text style={{ flex: 2, textAlign: 'right', marginRight: 8 }}>
                  {this.props.item.rank}
                </Text>
                <Image source={star} style={{ flex: 1, height: 40, marginRight: 8 }} />
              </View>
              <View style={{ flex: 1, borderRightWidth: 0.5, borderRightColor: 'black', display: 'flex', flexDirection: 'row' }}>
                <Text style={{ flex: 2, textAlign: 'right', marginRight: 8, fontSize: 20 }}>
                  {this.props.item.kind}
                </Text>
                <Image style={{ flex: 1, height: 40, marginRight: 8 }} />
              </View>
              <View style={{ flex: 1, display: 'flex', flexDirection: 'row' }}>
                <Text style={{ flex: 2, textAlign: 'right', marginRight: 8 }}>
                  {this.props.item.peopleNum}
                </Text>
                <Image source={people} style={{ flex: 1, height: 40, marginRight: 8 }} />
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
              <Text style={{ borderRadius: 16, padding: 8, marginBottom: 8, color: 'white' }}>
                {this.props.item.sentence}
              </Text>
            </View>
          </View>
        </LinearGradient>
      </View>
    )
  }
}


class FlatListItem2 extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeRowKey: null,
    };
  }
  render() {
    return (
      //making cards here
      <View style={{display:'flex', flexDirection:'column', borderWidth:1, borderColor:'black', padding:16, backgroundColor:'lightblue', margin:8}}>
        <Text style={{fontSize:48, textAlign:'right', color:'black'}}>
          :{this.props.item.name} 
        </Text>
        <Text style={{fontSize:24, textAlign:'right', color:'black'}}>
          {this.props.item.comment}
        </Text>
        <Text style={{fontSize:18, textAlign:'right', color:'black'}}>
        امتیاز کاربر:{this.props.item.rank} 
        </Text>
      </View>
    )
  }
}



export default class HomePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      a: true,
      first: 1,
      b: false,
      cash:0,
      username:'Al'
    }
  }
  async componentDidMount() {
    await this.setState({ a: true })
    await Parse.User.currentAsync().then(function (user) {
      userId = user.id
      userName= user.get(username) 
      a = 2;
    });
    await this.setState({ userId: userId});
  }

  async firstGap(item) {
    let flatListData = [];
    await this.setState({ first: 2, whichUrl: item.imageUrl, whichRank: item.rank, whichKind: item.kind, whichNum: item.peopleNum, whichName: item.name, whichAddress: item.address, whichdetail: item.detail, whichId: item.key })
    for(const result of item.comments){
      alert(result.name)
      flatListData.push({
        'key': result.name,
        'name': result.name,
        'comment': result.comment,
        'rank': result.rate

      })
      this.setState({list: flatListData})
    }
  }
  
  async checkIn(){
    //alert(this.state.whichName)
    var name = await this.state.whichName
    await Parse.User.currentAsync().then(function (user) {
      var Coin = Parse.Object.extend("Coin");
      var query = new Parse.Query(Coin);
      query.get(user.get('iid'))
      .then((coin) => {
        // The object was retrieved successfully.
        alert(name)
        if(coin.get('choose')==name){
          coin.set('coins',coin.get('coins')-1)
          alert('یه سکه از دست دادید :(')
        }else{
          coin.set('coins',coin.get('coins')+1)
          alert('تبریک یک سکه گرفتید:)')
        }
        coin.set('choose',name)
        return coin.save().then((coin1) => {
        },(error)=>{
          alert(error.message)
        })
      }, (error) => {
        alert(error.message)
        // The object was not retrieved successfully.
        // error is a Parse.Error with an error code and message.
      });
      
    });
  }

  addcomment() {
    var Catch = Parse.Object.extend("Agahi");
    var query = new Parse.Query(Catch);
    query.get(this.state.whichId)
      .then((gameScore) => {
        // The object was retrieved successfully.
        gameScore.addAllUnique("comments",[{
          name: this.state.username,
          comment: this.state.cm,
          rate: this.state.emtiaz
        }])
        return gameScore.save().then((gameScore1) => {

        })
      }, (error) => {
        alert(error.message)
        // The object was not retrieved successfully.
        // error is a Parse.Error with an error code and message.
      });
  }

  render() {
    if (this.state.a) {
      const myAsyncFunction = async () => {
        const myVariable = await getFlatListData();
        await this.setState({ flat: myVariable })
        //alert(myVariable[0].imageUrl);
      };
      myAsyncFunction();
      this.setState({ a: false })
    }
    if (this.state.b) {
      const myAsyncFunction = async () => {
        const myVariable = await getFlatListData2();
        await this.setState({ flat2: myVariable })
        //alert(myVariable[0].imageUrl);
      };
      myAsyncFunction();
      this.setState({ b: false })
    }
    if (this.state.first == 1) {
      return (
        //2AA1B7
        <View style={{ flex: 1, display: 'flex' }}>
          <View style={{ flex: 1, backgroundColor: '#2AA1B7', display: 'flex', flexDirection: 'row' }}>
            <View style={{ flex: 1, backgroundColor: '#2AA1B7' }}>
              <MenuBut navigation={this.props.navigation} />
            </View>
            <View style={{ flex: 2, display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', backgroundColor: '#2AA1B7' }}>
              <Text style={{ textAlign: 'right', color: 'white', fontSize: 32, marginLeft: 16 }}>
                سندباد
                    </Text>
            </View>
          </View>
          <View style={{ flex: 9 }}>
            <FlatList
              data={this.state.flat}
              renderItem={({ item, index }) => {  //item : each item object in dataList , index : Eg: 0,1,2,3,...
                // console.log(Item = ${item} , Index = ${index})
                return (<FlatListItem item={item} index={index} parentFlatList={this}></FlatListItem>);
              }}                    //When State changes, automatically re-render components
            >
            </FlatList>
          </View>
        </View>
      )
    } if (this.state.first == 2) {
      return (
        <View style={{ flex: 1, display: 'flex' }}>
          <View style={{ flex: 1, backgroundColor: '#2AA1B7', display: 'flex', flexDirection: 'row' }}>
            <View style={{ flex: 1, backgroundColor: '#2AA1B7' }}>
              <MenuBut navigation={this.props.navigation} />
            </View>
            <View style={{ flex: 2, display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', backgroundColor: '#2AA1B7' }}>
              <Text style={{ textAlign: 'right', color: 'white', fontSize: 32, marginLeft: '30%' }}>
                سندباد
                  </Text>
            </View>
            <View style={{ flex: 1, backgroundColor: '#2AA1B7' }}>
              <TouchableOpacity style={Styles.But} activeOpacity={0.3} onPress={() => this.setState({ first: 1 })} >
                <Image
                  style={Styles.MenuIcon}
                  source={back}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flex: 9, display: 'flex', flexDirection: 'column', width: '100%', alignItems: 'center' }}>
            <Image source={{ uri: this.state.whichUrl }} style={{ flex: 3, width: '95%', borderRadius: 30, marginTop: 16 }} />
            <View style={{ flex: 3, display: 'flex', flexDirection: 'column' }}>
              <View style={{ flex: 1, display: 'flex', flexDirection: 'row', marginTop: 8, borderWidth: 1, borderColor: 'black', borderRadius: 16, width: '100%' }}>
                <View style={{ flex: 1, borderRightWidth: 0.5, borderRightColor: 'black', display: 'flex', flexDirection: 'row' }}>
                  <Text style={{ flex: 2, textAlign: 'right', marginRight: 8 }}>
                    {this.state.whichRank}
                  </Text>
                  <Image source={star} style={{ flex: 1, height: 40, marginRight: 8 }} />
                </View>
                <View style={{ flex: 1, borderRightWidth: 0.5, borderRightColor: 'black', display: 'flex', flexDirection: 'row' }}>
                  <Text style={{ flex: 2, textAlign: 'right', marginRight: 8 }}>
                    {this.state.whichKind}
                  </Text>
                  <Image source={home} style={{ flex: 1, height: 40, marginRight: 8 }} />
                </View>
                <View style={{ flex: 1, display: 'flex', flexDirection: 'row' }}>
                  <Text style={{ flex: 2, textAlign: 'right', marginRight: 8 }}>
                    {this.state.whichNum}
                  </Text>
                  <Image source={people} style={{ flex: 1, height: 40, marginRight: 8 }} />
                </View>
              </View>
              <View style={{ flex: 2, display: 'flex', flexDirection: 'row', marginTop: 8, alignItems: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
                  <TouchableOpacity onPress={() => this.setState({ first: 4 })} style={{ width: widthAll * 0.25, height: widthAll * 0.25 }}>
                    <Image source={mapic} style={{ width: widthAll * 0.25, height: widthAll * 0.25 }} />
                  </TouchableOpacity>
                </View>
                <Text style={{ flex: 2, marginRight: 16, fontSize: 18 }}>
                  {this.state.whichAddress}
                </Text>
              </View>
              <View style={{ flex: 1, display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 20, textAlign: 'right' }}>
                  {this.state.whichdetail}
                </Text>
              </View>
            </View>




            <View style={{ flex: 0.75, flexDirection: 'row', display: 'flex', justifyContent: 'flex-end' }}>
              <TouchableOpacity style={{ flex: 1, height: '100%' }} onPress={()=>this.checkIn()}>
                <View style={{ flex: 1, backgroundColor: 'lightgreen', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <Image source={require('../pics/check.png')} style={{ width: 50, height: 50 }} />
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ flex: 1, height: '100%' }} onPress={() => this.setState({ first: 3, b: true })}>
                <View style={{ flex: 1, backgroundColor: 'lightblue', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <Image source={require('../pics/comment.png')} style={{ width: 50, height: 50 }} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )
    } if (this.state.first == 3) {
      return (
        <View style={{ flex: 1, display: 'flex' }}>
          <View style={{ flex: 1, backgroundColor: '#2AA1B7', display: 'flex', flexDirection: 'row' }}>
            <View style={{ flex: 1, backgroundColor: '#2AA1B7' }}>
              <MenuBut navigation={this.props.navigation} />
            </View>
            <View style={{ flex: 2, display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', backgroundColor: '#2AA1B7' }}>
              <Text style={{ textAlign: 'right', color: 'white', fontSize: 32, marginLeft: '30%' }}>
                سندباد
                  </Text>
            </View>
            <View style={{ flex: 1, backgroundColor: '#2AA1B7' }}>
              <TouchableOpacity style={Styles.But} activeOpacity={0.3} onPress={() => this.setState({ first: 2 })} >
                <Image
                  style={Styles.MenuIcon}
                  source={back}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flex: 9, display:'flex', flexDirection:'column' }}>
            <View style={{flex:6}}>
              <FlatList
                data={this.state.list}
                renderItem={({ item, index }) => {  //item : each item object in dataList , index : Eg: 0,1,2,3,...
                  // console.log(Item = ${item} , Index = ${index})
                  return (<FlatListItem2 item={item} index={index} parentFlatList={this}></FlatListItem2>);
                }}                    //When State changes, automatically re-render components
              >
              </FlatList>
            </View>
            <View style={{flex:3, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                <TextInput style={{flex:1, height: 5, width:'80%', borderColor:'skyblue', borderRadius: 20, borderWidth:1, marginTop: -5}} onChangeText={(cm)=>this.setState({cm})}/>
                <TextInput style={{flex:1, height: 5, width:'80%', borderColor:'skyblue' , borderRadius: 20, borderWidth:1, marginTop: -5}} onChangeText={(emtiaz)=>this.setState({emtiaz})}/>
                <Button style={{flex:1, width:'80%',  borderRadius: 20, borderWidth:1}} onPress={()=>this.addcomment()}>
                  <Text>
                    
                  </Text>
                  </Button>
            </View>
          </View>
        </View>
      )
    }
    if (this.state.first == 4) {
      return (
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ height: 60, backgroundColor: '#2AA1B7' }}>
            <MenuBut navigation={this.props.navigation} />

          </View>
          <View style={{ flex: 1, backgroundColor: '#2AA1B7' }}>
            <TouchableOpacity style={Styles.But} activeOpacity={0.3} onPress={() => this.setState({ first: 2 })} >
              <Image
                style={Styles.MenuIcon}
                source={back}
              />
            </TouchableOpacity>
          </View>
          <View style={{
            backgroundColor: '#EFEBEF',
            flexDirection: 'column',
            flexGrow: 5

          }}>
            <View style={{ flex: 2, borderColor: 'black', backgroundColor: '#BDB76B', shadowOpacity: 0.25, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
              <View style={Styles.mapContainer}>
                <MapView style={Styles.map}
                  customMapStyle={RetroMapStyle}
                  region={{
                    latitude: 35.70604347999999,
                    longitude: 51.34860591000006,
                    latitudeDelta: 0.1,
                    longitudeDelta: 0.1,
                  }}>

                  <Marker
                    coordinate={{
                      latitude: 35.70604347999999,
                      longitude: 51.34860591000006
                    }}
                    title={'دانشور'}
                    description={'آقای دانشور آلی'}
                  />
                </MapView>
              </View>
            </View>
            <Button style={{ flex: 0.5, borderColor: 'black', borderTopLeftRadius: 30, borderTopRightRadius: 30, backgroundColor: '#2AA1B7', shadowOpacity: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
              <Text style={Styles.fontTester}>من اینجام !</Text>
            </Button>


          </View>
        </View>

      )
    }
  }
}
// we found error 404 here!!
const Styles = StyleSheet.create({
  MenuIcon: {
    width: 30,
    height: 30
  },
  But: {
    zIndex: 0,
    top: 15,
    right: -40,
    width: 30,
    height: 30
  },
  container: {
    flex: 1,
    backgroundColor: '#FF4500',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    padding: 10
  },
  logo: {
    width: 120,
    height: 120
  },
  title: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9,
    padding: 20
  },
  infoContainer: {
    position: 'relative',
    left: 0,
    right: 0,
    bottom: 0,
    height: 200,
    padding: 10,
    // backgroundColor: 'yellow'
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    textAlign: 'right',
    paddingHorizontal: 20,
    color: 'white',
    borderColor: 'white',
    borderWidth: 3,
    marginTop: 4,
    borderRadius: 10
  },
  button: {
    height: 40,
    borderWidth: 3,
    marginTop: 4,
    borderRadius: 10
  },
  loginText: {
    textAlign: 'center',
    color: '#470010',
    marginTop: 5
  },
  map: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  mapContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  fontTester: {
    justifyContent: "center",
    fontSize: 30,
    fontFamily: 'serif',
    color: 'white',
    shadowOpacity: 1
  }
});

