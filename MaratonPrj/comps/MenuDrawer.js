import React from 'react'
import Parse from 'parse/react-native'
import {
    Platform,
    Dimensions,
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView
} from 'react-native'
import pic from '../pics/sinbad.png';
import people from '../pics/people.png';
import plus from '../pics/plus.png';
import mail from '../pics/mail.png';
import fav from '../pics/fav.png';
import money from '../pics/money.png';
import phone from '../pics/support.png';
import settings from '../pics/settings.png';
import vote from '../pics/vote.png';
import more from '../pics/more.png';
import aboutUs from '../pics/about.png';
import home from '../pics/home.png';
import { AsyncStorage } from 'react-native';
Parse.setAsyncStorage(AsyncStorage);
Parse.serverURL = 'https://parseapi.back4app.com/';
Parse.initialize("ImoJxywZ9Fre8V8vpFyHjwtpewOArNzOvtToDmcA","qW0uuIsFwvelWUUi1ORdo79ywIsVCryePRTTAQa5");

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


export default class MenuDrawer extends React.Component {
    navLink(nav, text, icon) {
        return (
            <TouchableOpacity style={{ height: 50, flexDirection: 'row', borderBottomWidth:0.5, borderBottomColor:'white' }} onPress={() => this.props.navigation.navigate(nav)}>
                <Image style={{ width: 30, height: 30, marginLeft: 10, marginTop: 10 }} source={icon} />
                <Text style={styles.link}>{text}</Text>
            </TouchableOpacity>
        )
    }

    async cash(){
        await this.setState({ a: true })
        await Parse.User.currentAsync().then(function (user) {
        userId = user.id
        a = 2;
        });
        await this.setState({ userId: userId });
        await Parse.User.currentAsync().then(function (user) {
            var Coin = Parse.Object.extend("Coin");
            var query = new Parse.Query(Coin);
            query.get(user.get('iid'))
            .then((coin) => {
              // The object was retrieved successfully.
              var b= 'شما ' +coin.get('coins')+' سکه دارید.'   
              alert(b)
            },(error)=>{
                alert(error.message)
              })
            }, (error) => {
              alert(error.message)
              // The object was not retrieved successfully.
              // error is a Parse.Error with an error code and message.
            });
            
          };

    LogOut() {
        Parse.User.logOut().then(() => {
            Parse.User.currentAsync().then(function (user) {
                RNRestart.Restart();
            });
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.scrollview}>
                    <View style={styles.topLinks}>
                        <View style={styles.profile}>
                            <View style={styles.profileImage}>
                                <Image style={styles.img} source={pic} />
                            </View>
                            <View style={styles.profileText}>
                                <Text style={styles.name}>دانشور آملی</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.bottomLinks}>
                        {this.navLink("No", "نام رفقا", people)}
                        {this.navLink("Suggest", "پیشنهاد مکان", plus)}
                        {this.navLink("No", "صندوق", mail)}
                        {this.navLink("No", "علاقه مندی ها", fav)}
                        <TouchableOpacity style={{ height: 50, flexDirection: 'row', borderBottomWidth:0.5, borderBottomColor:'white' }} onPress={() => this.cash()}>
                            <Image style={{ width: 30, height: 30, marginLeft: 10, marginTop: 10 }} source={money} />
                            <Text style={styles.link}>وضع مالی</Text>
                        </TouchableOpacity>
                        {this.navLink("No", "پشتیبانی", phone)}
                        {this.navLink("No", 'تنظیمات', settings)}
                        {this.navLink("No", 'رای به ما', vote)}
                        {this.navLink("No", 'بیش تر', more)}
                        {this.navLink("No", 'درباره ما', aboutUs)}
                        {this.navLink("Home", 'خانه', home)}
                        <TouchableOpacity style={{ height: 50, flexDirection: 'row' }} onPress={() => this.LogOut()}>
                            <Image style={{ width: 30, height: 30, marginLeft: 10, marginTop: 10 }} source={pic} />
                            <Text style={styles.link}>Log out!</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <Text style={styles.description}>sandbad</Text>
                    <Text style={styles.version}>v1.0</Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'lightgray',

    },
    scrollview: {

    },
    topLinks: {
        height: 150,
        backgroundColor: '#F7BC1E'
    },
    profile: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 20,
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
    },
    profileImage: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,

    },
    img: {
        height: 70,
        width: 70,
        borderRadius: 50,

    },
    profileText: {
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    name: {
        fontSize: 20,
        paddingBottom: 5,
        color: 'white',
        textAlign: 'left'
    },
    bottomLinks: {
        flex: 1,
        backgroundColor: '#2AA1B7',
        paddingTop: 10,
        paddingBottom: 450
    },
    link: {
        flex: 1,
        fontSize: 20,
        padding: 6,
        paddingLeft: 14,
        margin: 5,
        textAlign: 'left',
    },
    footer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderTopColor: 'lightgray'
    },
    description: {
        flex: 1,
        alignItems: 'flex-start',
        fontSize: 20,
        marginLeft: 20,
        color: 'gray',
        borderRightWidth: 1,
        borderRightColor: 'lightgray',
        paddingRight: '15%',
    },
    version: {
        flex: 1,
        color: 'gray',
        paddingLeft: '5%'
    }
});
