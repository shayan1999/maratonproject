import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';

export default class Suggest extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{
                    backgroundColor: 'grey',
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    height: 0
                }}>
                </View>

                <View style={{
                    backgroundColor: 'grey',
                    flexDirection: 'column',
                    flexGrow: 5

                }}>
                    <LinearGradient colors={['#2AA1B7', '#3b5998', '#192f6a']} style={styles.linearGradient}>
                        <View style={{
                            flex: 1, borderColor: 'black', borderRadius: 10, shadowOpacity: 0.25, flexDirection: 'column', alignItems: 'center', justifyContent: 'center'
                        }}>
                            <Image style={{ height: 90, width: 90 }} source={require('../pics/sinbad.png')}></Image>
                            <Text style={{ fontSize: deviceHeight * 0.18, marginTop: '1%', color: 'white' }}> به زودی ...</Text>
                        </View>
                    </LinearGradient>
                </View >


            </View >
        )
    }
}
const styles = StyleSheet.create({
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
});