import React, { Fragment, Component } from 'react';
import {
  View,
  Image,
  StyleSheet
} from 'react-native';
import Pic from '../pics/sinbad.png'

class SplashScreen extends Component {
  static navigationOptions = {
    header: null,
    title: null
  };
  constructor(props) {
    super(props);
    this.state = {
      timing: 0,
    };
    setInterval(() => (
      this.setState(previousState => (
        { timing: previousState.isShowingText + 10 }
      ))
    ), 2000);
  }


  render() {
    if (this.state.timing < 5000) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'lightorange' }}>
          <Image style={{ width: 250, height: 250, borderRadius: 125 }} source={Pic} />
        </View>
      )
    } else {
      this.props.navigation.dispatch({
        type: 'ReplaceCur',
        routeName: 'LogIn',
        key: 'LogIIn'
      })
      return (null)
    }
  }
}

export default SplashScreen;