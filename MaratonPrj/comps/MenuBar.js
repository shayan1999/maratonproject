import React from 'react'
import {StyleSheet, Image, TouchableOpacity, View} from 'react-native'
import menu from '../pics/menu.png'

export default class MenuBar extends React.Component {
    render(){
        return(
            <TouchableOpacity style={styles.But} activeOpacity={0.3} onPress={() => this.props.navigation.toggleDrawer()}>
                <Image
                style={styles.MenuIcon}
                source={menu}
                />
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({
    MenuIcon:{
        width:30,
        height:30
    },
    But:{
        zIndex:0,
        top:20,
        left:20,
        width:30,
        height:30
    }
});