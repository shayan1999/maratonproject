import React, { Component } from 'react';
import Parse from 'parse/react-native';
import { AsyncStorage } from 'react-native';
import { ToastAndroid } from 'react-native';
Parse.setAsyncStorage(AsyncStorage);
Parse.serverURL = 'https://parseapi.back4app.com/';
Parse.initialize("ImoJxywZ9Fre8V8vpFyHjwtpewOArNzOvtToDmcA", "qW0uuIsFwvelWUUi1ORdo79ywIsVCryePRTTAQa5");


import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert,
    Animated,
    Easing
} from 'react-native';

var userId;

export default class Login extends Component {





    static navigationOptions = {
        header: null,
        title: null
    }

    constructor(props) {
        super(props);



        state = {
            email: '',
            password: '',
            user: ''
        }
    }


    async myLogIn() {
        //(this.state.email)
        var user = await Parse.User
            .logIn(this.state.email, this.state.password).then(function (user) {
                Parse.User.currentAsync().then(function (user) {
                    //  alert(user.id)
                });
            }).catch(function (error) {
                alert(error.message);
            })
        await Parse.User.currentAsync().then(function (user) {
            userId = user.id
        });
        if (userId != null) {
            this.props.navigation.dispatch({
                type: 'ReplaceCur',
                routeName: 'Side',
                Params: { userId: userId },
                key: 'Sidee'
            })
        }
    }
    async componentDidMount() {
        await Parse.User.currentAsync().then(function (user) {
            userId = user.id
            a = 2;
        });
        await this.setState({ userId: userId });
    }

    render() {
        if (userId != null) {
            this.props.navigation.dispatch({
                type: 'ReplaceCur',
                routeName: 'Side',
                key: 'Sidee'
            })
            return null
        } else {
            return (
                <View style={styles.container}>
                    <View style={styles.biginput}>
                        <Text style={styles.text}>  username</Text>
                        <View style={styles.inputcontainer}>
                            <Image style={styles.inputIcon} />
                            <TextInput style={styles.input}
                                onChangeText={(email) => this.setState({ email })}
                                keyboardType="email-address"
                                underlineColorAndroid='transparent'
                            />
                        </View>
                    </View>
                    <View style={styles.biginput}>
                        <Text style={styles.text}>  password</Text>
                        <View style={styles.inputcontainer}>
                            <Image style={styles.inputIcon} />
                            <TextInput style={styles.input}
                                onChangeText={(password) => this.setState({ password })}

                                keyboardType="email-address"
                                underlineColorAndroid='transparent'
                            />
                        </View>
                    </View>
                    <TouchableHighlight style={styles.touchable}>
                        <Text style={styles.text1}>forgot your password?</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.touchable, styles.Button}
                        onPress={() => this.myLogIn()}>
                        <Text style={styles.text1}>  login</Text>
                    </TouchableHighlight>

                </View>
            )
        }

    }

}
const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#2AA1B7',

    },

    inputIcon: {

    }
    ,




    input: {
        alignItems: 'center'

    },
    TextInput: {
        position: 'absolute'
    },
    inputcontainer: {
        borderBottomColor: '#D8D7D8',
        backgroundColor: '#F9B10B',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 40,
        marginBottom: 20,
        flexDirection: 'column',
        alignItems: 'center',
        marginLeft: 50


    },
    biginput: {
        flexDirection: 'column'
    },
    text: {
        color: 'white',
        fontSize: 16,
        marginLeft: 40
    },
    touchable: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,


    },
    text1: {
        color: 'white',
        fontSize: 16,

        marginLeft: 90,
        marginTop: 5
    }
    ,


    Button: {
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 40,
        marginBottom: 20,
        backgroundColor: "#00b5ec",
        marginLeft: 50
    },

})
