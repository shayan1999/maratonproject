import React, { Component } from 'react';
import { CheckBox, Button } from 'native-base'
import { StyleSheet, Dimensions, View, Image, Text } from 'react-native'
import MapView from 'react-native-maps'
import { Marker } from 'react-native-maps'
import RetroMapStyle from '../MapStyles/RetroMapStyle'
import MenuBut from './MenuBar'

deviceHeight = Dimensions.get('window').height;
deviceWidth = Dimensions.get('window').width;


export default class Suggest extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: {
                latitude: 36.46,
                longitude: 52.35,
            }

        }
    }

    componentDidMount(){
        alert('موقعیت مورد نظرتان را انتخاب کرده و تایید کنید.')
    }

    getInitialState() {
        return {
            region: {
                latitude: 36.46,
                longitude: 52.35,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
        };
    }

    onRegionChange(region) {
        this.setState({ region });
    }

    onChangedd(e){
        this.setState({ x: e.nativeEvent.coordinate })
        var b= 'موقعیت انتخابی شما'+ this.state.x+' می باشد باتشکر از همکاری شما به زودی بررسی می شود'
        alert(b)
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ height: 60, backgroundColor: '#2AA1B7' }}>
                    <MenuBut navigation={this.props.navigation} />
                </View>
                <View style={{
                    backgroundColor: '#EFEBEF',
                    flexDirection: 'column',
                    flexGrow: 5

                }}>
                    <View style={{ flex: 2, borderColor: 'black', backgroundColor: '#BDB76B', shadowOpacity: 0.25, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <View style={Styles.mapContainer}>
                            <MapView style={Styles.map}
                                initialRegion={{
                                    latitude: 36.46,
                                    longitude: 52.35,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}>
                                <Marker draggable
                                    coordinate={this.state.x}
                                    onDragEnd={(e) => this.onChangedd(e)}

                                />
                            </MapView>
                        </View>
                    </View>
                    <Button style={{ flex: 0.5, borderColor: 'black', borderTopLeftRadius: 30, borderTopRightRadius: 30, backgroundColor: '#2AA1B7', shadowOpacity: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={Styles.fontTester}>ثبت مکان </Text>
                    </Button>


                </View>
            </View>






        );
    }
}


const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FF4500',
        flexDirection: 'column'
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        padding: 10
    },
    logo: {
        width: 120,
        height: 120
    },
    title: {
        color: 'white',
        fontSize: 18,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0.9,
        padding: 20
    },
    infoContainer: {
        position: 'relative',
        left: 0,
        right: 0,
        bottom: 0,
        height: 200,
        padding: 10,
        // backgroundColor: 'yellow'
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        textAlign: 'right',
        paddingHorizontal: 20,
        color: 'white',
        borderColor: 'white',
        borderWidth: 3,
        marginTop: 4,
        borderRadius: 10
    },
    button: {
        height: 40,
        borderWidth: 3,
        marginTop: 4,
        borderRadius: 10
    },
    loginText: {
        textAlign: 'center',
        color: '#470010',
        marginTop: 5
    },
    map: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    mapContainer: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    fontTester: {
        justifyContent: "center",
        fontSize: 30,
        fontFamily: 'serif',
        color: 'white',
        shadowOpacity: 1
    }
})